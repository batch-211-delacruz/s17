// console.log("Hello World");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function getUserInfo(){
	let name = prompt("What is your name? ");
	let age = prompt("How old are you? ");
	let address = prompt("Where do you live? ");
	console.log("Hello, " + name);
	console.log("You are " + age + " years old.");
	console.log("You live in " + address);

	};
	getUserInfo();
	// alert("Hi! Please add the names of your friends.");


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/


	//second function here:
	function printFavoriteBands(){
		console.log("1. Greenday");
		console.log("2. Dream theater");
		console.log("3. Rage against the machine");
		console.log("4. Paramore");
		console.log("5. Eraserheads");
	}

	printFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

		Avengers infinity war 85% tomato meter 91% audience score
		Fast five 78% tomato meter 83% audience score
		Top Gun: Maverick 96% tomatometer 99% audience score
		Avengers End Game 94% tomatometer 90% audience score
		The school of rock 92% tomatometer 64% audience score
	
*/
	
	//third function here:
	function printFavoriteMovies(){
		console.log("1.Avengers infinity war");
		console.log("85% tomato meter 91% audience score");
		console.log("2.Fast five");
		console.log("78% tomato meter 83% audience score");
		console.log("3.Top Gun: Maverick");
		console.log("96% tomatometer 99% audience score");
		console.log("4.Avengers End Game");
		console.log("94% tomatometer 90% audience score");
		console.log("5.The school of rock");
		console.log("92% tomatometer 64% audience score");
	};
	printFavoriteMovies();
	alert("Thank you for your input!");
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

//printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();
//console.log(friend1);
//console.log(friend2);